package com.j25.designpatterns.behavioral.observerobservable.v2;

import java.util.Vector;

public class NewsStation {
    // lista obiektów zainteresowanych zmianą.
    private Vector<IWatcher> watchers = new Vector<>();

    public News currentNews;

    public void addWatcher(IWatcher w){
        watchers.add(w);
    }

    // concurrentmodificationexception
    public void removeWatcher(IWatcher w){
        watchers.remove(w);
    }

    public void newNews(final News news){
        this.currentNews = news;

//        rozgłoszenie wiadomości do watchersów.
        watchers.forEach(w -> w.update(news));
    }
}
