package com.j25.designpatterns.behavioral.observerobservable.v2;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class News {
    private String wiadomosc;

    // 0 niski priorytet (małe zainteresowanie)
    // 10 wysoki priorytet
    private int poziomWiadomosci;
}
