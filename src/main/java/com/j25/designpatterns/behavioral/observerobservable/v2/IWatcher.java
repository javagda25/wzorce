package com.j25.designpatterns.behavioral.observerobservable.v2;

public interface IWatcher {
    void update(News n);
}
