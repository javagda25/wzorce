package com.j25.designpatterns.behavioral.observerobservable.v3;

import java.util.Observable;
import java.util.Observer;

public class KGB implements Observer {

    @Override
    public void update(Observable newsStation, Object arg) {
        System.out.println("KGB: " + arg);
    }
}
