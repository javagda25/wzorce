package com.j25.designpatterns.behavioral.observerobservable;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;

public class NewsStation {
    // lista obiektów zainteresowanych zmianą.
    public SimpleObjectProperty<News> newsProperty = new SimpleObjectProperty<>();

    public void dodajWatchera(ChangeListener<News> watcher){
        newsProperty.addListener(watcher);
    }

    public void usunWatchera(ChangeListener<News> watcher){
        newsProperty.removeListener(watcher);
    }

    public void newNews(News news){
        // roześlij informację o zmianie
        //
        newsProperty.setValue(news);
        //
        // ZAMIAST:
        // setChanged()
        // notifyObservers()
    }
}
