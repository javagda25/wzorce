package com.j25.designpatterns.behavioral.observerobservable.v1;

public class Watcher {
    private int poziomZainteresowania;
    private String imie;

    public Watcher(int poziomZainteresowania, String imie) {
        this.poziomZainteresowania = poziomZainteresowania;
        this.imie = imie;
    }

    public void notifyAboutNews(News news) {
        if(news.getPoziomWiadomosci() > poziomZainteresowania){
            System.err.println(imie + ": OMG! O nie!  " + news.getWiadomosc());
        }else{
            System.out.println(imie + ": Etam, wiadomosci: " + news.getWiadomosc());
        }
    }
}
