package com.j25.designpatterns.behavioral.observerobservable.v1;

import java.util.ArrayList;
import java.util.List;

public class NewsStation {
    // lista obiektów zainteresowanych zmianą.
    private List<Watcher> watchers = new ArrayList<Watcher>();

    public News currentNews;

    public void addWatcher(Watcher w){
        watchers.add(w);
    }

    public void removeWatcher(Watcher w){
        watchers.remove(w);
    }

    public void newNews(final News news){
        this.currentNews = news;

//        rozgłoszenie wiadomości do watchersów.
        watchers.forEach(w -> w.notifyAboutNews(news));
    }
}
