package com.j25.designpatterns.behavioral.observerobservable;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class KGB implements ChangeListener<News> {

    @Override
    public void changed(ObservableValue<? extends News> observable, News oldValue, News newValue) {
        System.out.println("KGB: " + newValue);
    }
}
