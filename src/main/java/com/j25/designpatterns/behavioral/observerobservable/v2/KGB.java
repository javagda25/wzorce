package com.j25.designpatterns.behavioral.observerobservable.v2;

public class KGB implements IWatcher {
    @Override
    public void update(News n) {
        System.out.println("KGB: " + n);
    }
}
