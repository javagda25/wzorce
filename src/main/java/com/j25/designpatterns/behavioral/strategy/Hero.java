package com.j25.designpatterns.behavioral.strategy;

import com.j25.designpatterns.behavioral.strategy.strategie.IStrategia;

public class Hero {
    private String name;

    private IStrategia strategia;

    public Hero(String name) {
        this.name = name;
    }

    public void setStrategia(IStrategia strategia) {
        this.strategia = strategia;
    }

    // bohater który walczy mieczem
    public void walcz(){
        strategia.walcz();
    }
}
