package com.j25.designpatterns.behavioral.strategy.strategie;

public class StrategiaWalkiMieczem implements IStrategia{
    @Override
    public void walcz() {
        System.out.println("Ciach ciach, mieczem go!");
    }
}
