package com.j25.designpatterns.behavioral.strategy;

import com.j25.designpatterns.behavioral.strategy.strategie.StrategiaWalkiMieczem;
import com.j25.designpatterns.behavioral.strategy.strategie.StrategiaWalkiRózgą;
import com.j25.designpatterns.behavioral.strategy.strategie.StrategiaWalkiUkiem;

public class Main {
    public static void main(String[] args) {
        Hero h = new Hero("Janusz");

        h.setStrategia(new StrategiaWalkiMieczem());
        h.walcz();

        h.setStrategia(new StrategiaWalkiUkiem());
        h.walcz();
        h.walcz();
        h.walcz();

        h.setStrategia(new StrategiaWalkiMieczem());
        h.walcz();

        h.setStrategia(new StrategiaWalkiRózgą());
        h.walcz();
    }
}
