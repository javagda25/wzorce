package com.j25.designpatterns.behavioral.strategy_proxy;

import com.j25.designpatterns.behavioral.strategy_proxy.styl_ruchu.StylRuchuPoruszajSiePoSkosie;
import com.j25.designpatterns.behavioral.strategy_proxy.styl_ruchu.StylRuchuPoruszajSieWDol;
import com.j25.designpatterns.behavioral.strategy_proxy.styl_ruchu.StylRuchuPoruszajSieWPrawo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Postac postac = new Postac();

        Scanner scanner = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking) {
            String komenda = scanner.nextLine();

            if (komenda.equalsIgnoreCase("podmien")) {
                komenda = scanner.nextLine();

                if (komenda.equalsIgnoreCase("dol")) {
                    postac.setStyl(new StylRuchuPoruszajSieWDol());
                } else if (komenda.equalsIgnoreCase("prawo")) {
                    postac.setStyl(new StylRuchuPoruszajSieWPrawo());
                } else if (komenda.equalsIgnoreCase("skos")) {
                    postac.setStyl(new StylRuchuPoruszajSiePoSkosie());
                }
            } else if (komenda.equalsIgnoreCase("quit")) {
                break;
            } else {
                postac.porusz();
            }
        }
    }
}
