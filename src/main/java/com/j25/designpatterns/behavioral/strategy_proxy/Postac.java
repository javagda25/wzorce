package com.j25.designpatterns.behavioral.strategy_proxy;

import com.j25.designpatterns.behavioral.strategy_proxy.styl_ruchu.IStylRuchu;
import lombok.Data;

@Data
public class Postac implements IPostac {
    private int x = 0; // przesuń w prawo
    private int y = 0; // przesun w dol
    private final static char POSTAĆ = 'X';

    private IStylRuchu styl;

    public void setStyl(IStylRuchu styl) {
        this.styl = styl;
        this.styl.ustawPostac(this);
    }

    public void porusz() {
        styl.poruszSie();
        wypiszPozycje();
    }

    public void wypiszPozycje() {
        System.out.print("###########################################");

        for (int i = 0; i < y; i++) {
            System.out.println();
        }
        for (int i = 0; i < x; i++) {
            System.out.print(" ");
        }
        System.out.println(POSTAĆ);
    }
}
