package com.j25.designpatterns.behavioral.strategy_proxy.styl_ruchu;

import com.j25.designpatterns.behavioral.strategy_proxy.IPostac;

public abstract class IStylRuchu {
    protected IPostac postac;

    public void ustawPostac(IPostac postac){
        this.postac = postac;
    }

    public abstract void poruszSie();
}
