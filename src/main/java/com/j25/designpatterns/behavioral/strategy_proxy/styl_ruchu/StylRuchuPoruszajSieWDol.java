package com.j25.designpatterns.behavioral.strategy_proxy.styl_ruchu;

public class StylRuchuPoruszajSieWDol extends IStylRuchu {

    @Override
    public void poruszSie() {
        postac.setY(postac.getY() + 1);
    }
}
