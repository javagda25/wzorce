package com.j25.designpatterns.behavioral.strategy_proxy.styl_ruchu;

public class StylRuchuPoruszajSieWPrawo extends IStylRuchu{

    @Override
    public void poruszSie() {
        postac.setX(postac.getX() + 1);
    }
}
