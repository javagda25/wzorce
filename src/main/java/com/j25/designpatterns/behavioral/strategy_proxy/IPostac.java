package com.j25.designpatterns.behavioral.strategy_proxy;

public interface IPostac {
    int getX();
    int getY();
    void setX(int x);
    void setY(int y);
}
