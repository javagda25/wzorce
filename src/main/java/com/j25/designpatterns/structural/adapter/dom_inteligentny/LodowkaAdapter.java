package com.j25.designpatterns.structural.adapter.dom_inteligentny;

import com.j25.designpatterns.structural.adapter.IElementDomuInteligentnego;

public class LodowkaAdapter implements IElementDomuInteligentnego {
    private final Lodowka lodowka;

    public LodowkaAdapter(Lodowka lodowka) {
        this.lodowka = lodowka;
    }

    @Override
    public void turnDeviceOn() {
        lodowka.on();
    }

    @Override
    public void turnDeviceOff() {
        lodowka.off();
    }

    @Override
    public String getMeCoffe() throws UnsupportedOperationException{
        throw new UnsupportedOperationException();
    }
}
