package com.j25.designpatterns.structural.adapter;

public interface IElementDomuInteligentnego {
    void turnDeviceOn();
    void turnDeviceOff();
    String getMeCoffe() throws UnsupportedOperationException;
}
