package com.j25.designpatterns.structural.adapter.dom_inteligentny;

public class Czajnik {
    public void uruchomSie() {
        System.out.println("uruchamiam czajnik");

    }

    public void zamknijSie() {
        System.out.println("zamykam czajnik");
    }
}
