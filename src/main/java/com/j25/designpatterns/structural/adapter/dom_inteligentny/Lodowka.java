package com.j25.designpatterns.structural.adapter.dom_inteligentny;

public class Lodowka {
    public void on() {
        System.out.println("Samsung on!");
    }

    public void off() {
        System.out.println("Samsung off!");
    }
}
