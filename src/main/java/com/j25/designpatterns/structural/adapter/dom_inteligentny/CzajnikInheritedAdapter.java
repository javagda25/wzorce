package com.j25.designpatterns.structural.adapter.dom_inteligentny;

import com.j25.designpatterns.structural.adapter.IElementDomuInteligentnego;

public class CzajnikInheritedAdapter extends Czajnik implements IElementDomuInteligentnego {

    @Override
    public void turnDeviceOn() {
        uruchomSie();
    }

    @Override
    public void turnDeviceOff() {
        zamknijSie();
    }

    @Override
    public String getMeCoffe() throws UnsupportedOperationException{
        throw new UnsupportedOperationException();
    }
}
