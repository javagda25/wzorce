package com.j25.designpatterns.structural.adapter.dom_inteligentny;

import com.j25.designpatterns.structural.adapter.IElementDomuInteligentnego;

public class CzajnikAdapter implements IElementDomuInteligentnego {
    private final Czajnik czajnik;

    public CzajnikAdapter(Czajnik czajnik) {
        this.czajnik = czajnik;
    }

    @Override
    public void turnDeviceOn() {
        czajnik.uruchomSie();
    }

    @Override
    public void turnDeviceOff() {
        czajnik.zamknijSie();
    }

    @Override
    public String getMeCoffe() throws UnsupportedOperationException{
        throw new UnsupportedOperationException();
    }
}
