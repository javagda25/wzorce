package com.j25.designpatterns.structural.adapter;

import com.j25.designpatterns.structural.adapter.dom_inteligentny.Czajnik;
import com.j25.designpatterns.structural.adapter.dom_inteligentny.CzajnikAdapter;
import com.j25.designpatterns.structural.adapter.dom_inteligentny.Lodowka;
import com.j25.designpatterns.structural.adapter.dom_inteligentny.LodowkaAdapter;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        String name = "Pablo Emilio Escobar Garvilia";
        String[] nameSplitted = name.split(" ", 2);

        System.out.println(nameSplitted[0]);
        System.out.println(nameSplitted[1]);

//        Logger.getLogger(Main.class.getName()).log(Level.INFO, "Wiadomosc", );

//        List<IElementDomuInteligentnego> dom = new ArrayList<>();
//        dom.add(new CzajnikAdapter(new Czajnik()));
//        dom.add(new LodowkaAdapter(new Lodowka()));
//
//        for (IElementDomuInteligentnego elementDomuInteligentnego : dom) {
//            elementDomuInteligentnego.turnDeviceOn();
//        }

        long timestampLong = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(timestampLong);

        LocalDateTime dateTime = LocalDateTime.from(timestamp.toInstant());


    }
}
