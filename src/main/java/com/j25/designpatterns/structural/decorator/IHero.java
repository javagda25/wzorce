package com.j25.designpatterns.structural.decorator;

public interface IHero {
    int getAttackPoints();

    int getDefencePoints();

    int getHealthPoints();

    void attack(int points) throws Hero;
}
