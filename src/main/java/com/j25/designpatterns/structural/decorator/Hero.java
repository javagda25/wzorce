package com.j25.designpatterns.structural.decorator;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public final class Hero extends Throwable /*implements IHero */{
    private String name;
    private int attackPoints;
    private int defencePoints;
    private int healthPoints;

    public static Hero createHero(String name) {
        return new Hero(name, 100, 200, 100);
    }

    public void attack(int points) {
        int ileZyciaOdjac = points / getDefencePoints();

        healthPoints -= ileZyciaOdjac;
    }
}
