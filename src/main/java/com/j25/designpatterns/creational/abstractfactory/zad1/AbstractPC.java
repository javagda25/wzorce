package com.j25.designpatterns.creational.abstractfactory.zad1;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AbstractPC {
    private String name;
    private ComputerBrand brand;
    private double cpuPower;
    private double gpuPower;
    private boolean isOverclocked;
}
