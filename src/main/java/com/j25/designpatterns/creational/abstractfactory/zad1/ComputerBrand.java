package com.j25.designpatterns.creational.abstractfactory.zad1;

public enum ComputerBrand {
    ASUS,
    HP,
    SAMSUNG,
    APPLE
}
