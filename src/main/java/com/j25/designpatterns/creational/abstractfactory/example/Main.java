package com.j25.designpatterns.creational.abstractfactory.example;
import com.j25.designpatterns.creational.abstractfactory.example.car.CarFactoryAudi;
import com.j25.designpatterns.creational.abstractfactory.example.car.ICar;
import com.j25.designpatterns.creational.abstractfactory.example.car.MercedesCar;

public class Main {
    public static void main(String[] args) {

        ICar a3_1 = CarFactoryAudi.createA3("Biały");
        ICar a3_2 = CarFactoryAudi.createA3("Czerwony");

        System.out.println(a3_1.getHorsepower());
        System.out.println(a3_1.getColor());

        ICar bęc = MercedesCar.createMercedesCLS();

        // nie da się
//        MercedesCar crr = new MercedesCar();

    }
}
