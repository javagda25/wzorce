package com.j25.designpatterns.creational.abstractfactory.example.car;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MercedesCar extends Car {
    private boolean celownik;

    private MercedesCar(String color, double horsepower, String manufacturer, String model, boolean turbo, double engineCapacity, boolean celownik) {
        super(color, horsepower, manufacturer, model, turbo, engineCapacity);
        this.celownik = celownik;
    }

    // metoda factory - publiczna statyczna metoda stworzeniowa, ktora zwraca gotowy obiekt.
    //
    public static MercedesCar createMercedesCLS(){
        return new MercedesCar("Black, red stripe", 6969, "Mercedes Bęc", "CLS", true, 2.0, true);
    }

}
