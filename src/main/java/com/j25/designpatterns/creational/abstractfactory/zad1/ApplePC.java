package com.j25.designpatterns.creational.abstractfactory.zad1;

import lombok.AllArgsConstructor;

public class ApplePC extends AbstractPC {
    public ApplePC(String name, double cpuPower, double gpuPower, boolean isOverclocked) {
        super(name, ComputerBrand.APPLE, cpuPower, gpuPower, isOverclocked);
    }

    public static ApplePC createAir13() {
        return new ApplePC("Air 13", 10, 1, false);
    }

    public static ApplePC createMacbookPro15() {
        return new ApplePC("McBug PRO", 50, 10, true);
    }
}
