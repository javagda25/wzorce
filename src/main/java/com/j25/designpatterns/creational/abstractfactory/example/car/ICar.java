package com.j25.designpatterns.creational.abstractfactory.example.car;

public interface ICar {
    double getHorsepower();
    String getColor();
    String getManufacturer();
    String getModel();
    boolean isTurbo();
    double getEngineCapacity();

}
