package com.j25.designpatterns.creational.abstractfactory.example.car;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
class Car implements ICar{
    private String color;
    private double horsepower;
    private String manufacturer;
    private String model;
    private boolean turbo;
    private double engineCapacity;


}
