package com.j25.designpatterns.creational.abstractfactory.example.car;


// Abstract factory - klasa która jest zbiorczą klasą z wieloma metodami "factory"
public abstract class CarFactoryAudi {

    // metoda factory - pojedyncza metoda stworzeniowa
    public static ICar createA3(String color){
        return new Car(color, 130, "Audi", "A3", false, 2.0);
    }

    // metoda factory
    public static ICar createS3(String color){
        return new Car(color, 280, "Audi", "S3", true, 2.1);
    }

}
