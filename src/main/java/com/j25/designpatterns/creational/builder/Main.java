package com.j25.designpatterns.creational.builder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Bohater.BohaterBuilder builder = Bohater.builder();

        // builder jest formą szablonu
        builder.imieMamy("Janina")
                .imieTaty("Misio")
                .poziomZycia(0)
                .stanowisko("Martwy");

        Set<Bohater> set = new HashSet<Bohater>();
        List<Bohater> list = new ArrayList<Bohater>();

        set.add(builder.build()); // obiekt 1
        set.add(builder.build()); // obiekt 2
        set.add(builder.build()); // obiekt 3
        set.add(builder.build()); // obiekt 4
        set.add(builder.build()); // obiekt 5

        System.out.println(set.size());
    }
}
