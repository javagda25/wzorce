package com.j25.designpatterns.creational.builder;

import lombok.Builder;
import lombok.ToString;

@Builder
public class Bohater {
    private String imie, nazwisko, imieMamy, imieTaty, imieBabci, imieDziadka, imieBabci2, imieDziadka2, imiePieska, imieKotka, ksywa, stanowisko;
    private int wiek, wzrost, rozmiarButa, waga, pesel, ilePodnoszeNaKlate, poziomZycia, punktyAtaku, punktyObrony, punktyMany, iloscBroniKtoraUniose;
    private boolean czyŻywy;

    public Bohater(String imie, String nazwisko, String imieMamy, String imieTaty, String imieBabci, String imieDziadka, String imieBabci2, String imieDziadka2, String imiePieska, String imieKotka, String ksywa, String stanowisko, int wiek, int wzrost, int rozmiarButa, int waga, int pesel, int ilePodnoszeNaKlate, int poziomZycia, int punktyAtaku, int punktyObrony, int punktyMany, int iloscBroniKtoraUniose, boolean czyŻywy) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.imieMamy = imieMamy;
        this.imieTaty = imieTaty;
        this.imieBabci = imieBabci;
        this.imieDziadka = imieDziadka;
        this.imieBabci2 = imieBabci2;
        this.imieDziadka2 = imieDziadka2;
        this.imiePieska = imiePieska;
        this.imieKotka = imieKotka;
        this.ksywa = ksywa;
        this.stanowisko = stanowisko;
        this.wiek = wiek;
        this.wzrost = wzrost;
        this.rozmiarButa = rozmiarButa;
        this.waga = waga;
        this.pesel = pesel;
        this.ilePodnoszeNaKlate = ilePodnoszeNaKlate;
        this.poziomZycia = poziomZycia;
        this.punktyAtaku = punktyAtaku;
        this.punktyObrony = punktyObrony;
        this.punktyMany = punktyMany;
        this.iloscBroniKtoraUniose = iloscBroniKtoraUniose;
        this.czyŻywy = czyŻywy;
    }

    public static Bohater createPlayerHero() {
        return Bohater.builder().imie("Marian").poziomZycia(20).punktyMany(100).punktyObrony(100).build();
    }
}
