package com.j25.designpatterns.creational.singleton;

// wersja 4
// eager
public enum ConfigFileManager {
    INSTANCE;

    private ConfigFileManager(){
        System.out.println("Stworzyłem");
    }

    private void otworzPlik() {

    }

    private void zamknijPlik() {

    }

    public void wczytajPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

    public void zapiszPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

}
