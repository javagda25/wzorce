package com.j25.designpatterns.creational.singleton.versions;

// wersja 4
// eager
public enum ConfigFileManager_v4 {
    INSTANCE;

    private ConfigFileManager_v4(){
        System.out.println("Stworzyłem");
    }

    private void otworzPlik() {

    }

    private void zamknijPlik() {

    }

    public void wczytajPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

    public void zapiszPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

}
