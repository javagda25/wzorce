package com.j25.designpatterns.creational.singleton.versions;

public class ConfigFileManager_v3 {
    // wersja 3
    // lazy synchronized
    private static ConfigFileManager_v3 INSTANCE;

    private ConfigFileManager_v3() {
        System.out.println("Stworzyłem");
    }

    public static void doSomething(){
        System.out.println("Something");
    }
    // w1
    // w2
    public static ConfigFileManager_v3 getInstance() {
        if (INSTANCE == null) {
            synchronized (ConfigFileManager_v3.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ConfigFileManager_v3();
                }
            }
        }
        return INSTANCE;
    }


    private void otworzPlik() {

    }

    private void zamknijPlik() {

    }

    public void wczytajPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

    public void zapiszPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

}
