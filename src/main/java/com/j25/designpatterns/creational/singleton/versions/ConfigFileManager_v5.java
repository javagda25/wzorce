package com.j25.designpatterns.creational.singleton.versions;

public class ConfigFileManager_v5 {
    // wersja 2
    // eager - instancja tworzy się przed pierwszym zastosowaniem (po załadowaniu klasy)

    private static ConfigFileManager_v5 INSTANCE;

    private ConfigFileManager_v5() {
        System.out.println("Stworzyłem");
    }

    public static ConfigFileManager_v5 getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ConfigFileManager_v5();
        }
        return INSTANCE;
    }


    private void otworzPlik() {

    }

    private void zamknijPlik() {

    }

    public void wczytajPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

    public void zapiszPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

}
