package com.j25.designpatterns.creational.singleton;

public class DatabaseConnection {
    private static DatabaseConnection ourInstance = new DatabaseConnection();

    public static DatabaseConnection getInstance() {
        return ourInstance;
    }

    private DatabaseConnection() {
    }
}
