package com.j25.designpatterns.creational.singleton;

import com.j25.designpatterns.creational.singleton.versions.ConfigFileManager_v1;
import com.j25.designpatterns.creational.singleton.versions.ConfigFileManager_v3;

public class Main {
    public static void main(String[] args) {
        // singleton - pojedyncza instancja obiektu pewnej klasy.

        System.out.println("v1");
        ConfigFileManager_v1.doSomething();
        System.out.println("v3");
        ConfigFileManager_v3.doSomething();
    }
}
