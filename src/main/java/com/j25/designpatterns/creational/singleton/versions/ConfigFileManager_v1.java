package com.j25.designpatterns.creational.singleton.versions;

// antywzorzec
public class ConfigFileManager_v1 {
    // wersja 1
    // eager - instancja tworzy się przed pierwszym zastosowaniem (po załadowaniu klasy)
    // lazy - przy użyciu
    public static final ConfigFileManager_v1 INSTANCE = new ConfigFileManager_v1();

    public static void doSomething(){
        System.out.println("Something");
    }

    private ConfigFileManager_v1() {
        System.out.println("Stworzyłem");
    }


    private void otworzPlik() {

    }

    private void zamknijPlik() {

    }

    public void wczytajPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

    public void zapiszPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

}
