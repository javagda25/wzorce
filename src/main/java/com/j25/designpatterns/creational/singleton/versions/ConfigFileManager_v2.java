package com.j25.designpatterns.creational.singleton.versions;

public class ConfigFileManager_v2 {
    // wersja 2
    // eager - instancja tworzy się przed pierwszym zastosowaniem (po załadowaniu klasy)

    private static final ConfigFileManager_v2 INSTANCE = new ConfigFileManager_v2();

    private ConfigFileManager_v2() {
        System.out.println("Stworzyłem");
    }

    public static ConfigFileManager_v2 getInstance() {
        return INSTANCE;
    }


    private void otworzPlik() {

    }

    private void zamknijPlik() {

    }

    public void wczytajPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

    public void zapiszPlik() {
        otworzPlik();
//        ...
        zamknijPlik();
    }

}
