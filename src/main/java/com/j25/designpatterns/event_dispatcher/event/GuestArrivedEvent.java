package com.j25.designpatterns.event_dispatcher.event;

import com.j25.designpatterns.event_dispatcher.actors.Guest;
import com.j25.designpatterns.event_dispatcher.dispatcher.Dispatcher;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestArrivedListener;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestLeftListener;

import java.util.List;

public class GuestArrivedEvent implements IEvent {
    private Guest g;

    public GuestArrivedEvent(Guest g) {
        this.g = g;
    }

    @Override
    public void execute() {
        List<IGuestArrivedListener> listenerList = Dispatcher.getInstance().getAllListenersOf(IGuestArrivedListener.class);
        for (IGuestArrivedListener iGuestArrivedListener : listenerList) {
            iGuestArrivedListener.guestArrived(g);
        }

    }
}
