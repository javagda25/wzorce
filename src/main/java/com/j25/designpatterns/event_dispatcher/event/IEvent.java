package com.j25.designpatterns.event_dispatcher.event;

public interface IEvent {
    void execute();
}
