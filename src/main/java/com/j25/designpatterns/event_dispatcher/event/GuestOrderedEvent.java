package com.j25.designpatterns.event_dispatcher.event;

import com.j25.designpatterns.event_dispatcher.Order;
import com.j25.designpatterns.event_dispatcher.actors.Guest;
import com.j25.designpatterns.event_dispatcher.dispatcher.Dispatcher;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestArrivedListener;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestOrderedListener;

import java.util.List;

public class GuestOrderedEvent implements IEvent {
    private Guest g;
    private Order order;

    public GuestOrderedEvent(Guest g, Order order) {
        this.g = g;
        this.order = order;
    }

    @Override
    public void execute() {
        List<IGuestOrderedListener> listenerList = Dispatcher.getInstance().getAllListenersOf(IGuestOrderedListener.class);
        for (IGuestOrderedListener iGuestArrivedListener : listenerList) {
            iGuestArrivedListener.guestOrdered(g, order);
        }

    }
}
