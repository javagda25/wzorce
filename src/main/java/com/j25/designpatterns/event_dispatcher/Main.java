package com.j25.designpatterns.event_dispatcher;

import com.j25.designpatterns.event_dispatcher.actors.Cashier;
import com.j25.designpatterns.event_dispatcher.actors.Chef;
import com.j25.designpatterns.event_dispatcher.actors.Guest;
import com.j25.designpatterns.event_dispatcher.actors.SecurityGuy;
import com.j25.designpatterns.event_dispatcher.dispatcher.Dispatcher;
import com.j25.designpatterns.event_dispatcher.event.GuestArrivedEvent;
import com.j25.designpatterns.event_dispatcher.event.GuestOrderedEvent;

public class Main {
    public static void main(String[] args) {
        new Chef();

        new Chef();

        new SecurityGuy();

        new Cashier();


        Dispatcher.getInstance().fireEvent(new GuestOrderedEvent(new Guest("Marian"), new Order("Frytki", 4.0)));
    }
}
