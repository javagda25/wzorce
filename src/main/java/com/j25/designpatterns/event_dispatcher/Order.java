package com.j25.designpatterns.event_dispatcher;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Order {
    private String dish;
    private double price;
}
