package com.j25.designpatterns.event_dispatcher.actors;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Guest {
    private String name;

}
