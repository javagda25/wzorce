package com.j25.designpatterns.event_dispatcher.actors;

import com.j25.designpatterns.event_dispatcher.Order;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestOrderedListener;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestPaidListener;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestReceivedOrderListener;

import java.util.HashMap;
import java.util.Map;

public class Cashier extends IBaseClass implements IGuestOrderedListener, IGuestPaidListener {
    Map<Guest, Order> orders = new HashMap<>();

    @Override
    public void guestOrdered(Guest g, Order o) {
        System.out.println(g + " ordered " + o + "- cashier");
        orders.put(g, o);
    }

    @Override
    public void guestPaid(Guest g, double amount) {
        System.out.println(g + " ordered " + amount + "- cashier");
        if (orders.get(g).getPrice() < amount) {
            System.out.println("Gościu nie dopłacił");
        } else {
            System.out.println("Hajs się zgadza");
            orders.remove(g);
        }
    }
}
