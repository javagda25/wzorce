package com.j25.designpatterns.event_dispatcher.actors;

import com.j25.designpatterns.event_dispatcher.Order;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestLeftListener;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestPaidListener;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestReceivedOrderListener;

import java.util.ArrayList;
import java.util.List;

public class SecurityGuy extends IBaseClass  implements IGuestReceivedOrderListener, IGuestPaidListener, IGuestLeftListener {
    List<Guest> listGuestNotPaid = new ArrayList<>();

    @Override
    public void guestPaid(Guest g, double amount) {
        listGuestNotPaid.remove(g);
        System.out.println("Usuwam goscia ktory zaplacil za zamowienie: " + g);
    }

    @Override
    public void guestReceivedOrder(Guest g, Order o) {
        listGuestNotPaid.add(g);
        System.out.println();
    }

    @Override
    public void guestLeft(Guest g) {
        if (listGuestNotPaid.contains(g)) {
            System.err.println("Dzwonie na policje, nie zaplacil za obiadek. ): " + g);
        }
    }
}
