package com.j25.designpatterns.event_dispatcher.actors;

import com.j25.designpatterns.event_dispatcher.dispatcher.Dispatcher;

public abstract class IBaseClass {
    public IBaseClass() {
        Dispatcher.getInstance().register(this);
    }
}
