package com.j25.designpatterns.event_dispatcher.actors;

import com.j25.designpatterns.event_dispatcher.Order;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestOrderedListener;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestReceivedOrderListener;

import java.util.ArrayList;
import java.util.List;

public class Chef extends IBaseClass  implements IGuestOrderedListener, IGuestReceivedOrderListener {
    List<Order> ordersToMake = new ArrayList<>();

    @Override
    public void guestOrdered(Guest g, Order o) {
        ordersToMake.add(o);
        System.out.println("Dodaje zamowienie: " + o);
    }

    @Override
    public void guestReceivedOrder(Guest g, Order o) {
        ordersToMake.remove(o);
        System.out.println("Zrealizowałem zamowienie: " + o);
    }
}
