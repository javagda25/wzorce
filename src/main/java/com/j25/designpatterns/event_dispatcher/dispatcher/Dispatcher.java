package com.j25.designpatterns.event_dispatcher.dispatcher;

import com.j25.designpatterns.event_dispatcher.event.IEvent;
import com.j25.designpatterns.event_dispatcher.listeners.IGuestLeftListener;
import org.apache.commons.lang3.ClassUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Dispatcher {
    private static Dispatcher ourInstance = new Dispatcher();

    public static Dispatcher getInstance() {
        return ourInstance;
    }

    private Map<Class<?>, List<Object>> listeners = new HashMap<>();
    private ExecutorService thread = Executors.newSingleThreadExecutor();

    public void register(Object o) {
        List<Class<?>> interfejsyImplementowanePrzezO = ClassUtils.getAllInterfaces(o.getClass());
        for (Class<?> interfejs : interfejsyImplementowanePrzezO) {
            if (listeners.containsKey(interfejs)) {
                listeners.get(interfejs).add(o);
            } else {
                listeners.put(interfejs, new ArrayList<>());
                listeners.get(interfejs).add(o);
            }
        }
    }

    public <T> List<T> getAllListenersOf(Class<T> tClass) {
        return (List<T>) listeners.get(tClass);
    }

    private Dispatcher() {
    }

    public void fireEvent(IEvent iEvent) {
        thread.submit(new Runnable() {
            @Override
            public void run() {
                iEvent.execute();
            }
        });
    }

}
