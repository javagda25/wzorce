package com.j25.designpatterns.event_dispatcher.listeners;

import com.j25.designpatterns.event_dispatcher.actors.Guest;

public interface IGuestPaidListener {
    void guestPaid(Guest g, double amount);
}
