package com.j25.designpatterns.event_dispatcher.listeners;

import com.j25.designpatterns.event_dispatcher.Order;
import com.j25.designpatterns.event_dispatcher.actors.Guest;

public interface IGuestOrderedListener {
    void guestOrdered(Guest g, Order o);
}
